FROM ubuntu:focal
#SSH
#registry.gitlab.com/huezo/sshpass:latest

ENV DEBIAN_FRONTEND noninteractive

# Set the timezone.
ENV TZ=America/El_Salvador
RUN apt-get update 
RUN apt-get install -y tzdata 
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime 
RUN echo $TZ > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

#kubectl helm
ADD ./ /usr/bin/
RUN chmod -R +x /usr/bin/

#######
RUN apt-get update && apt-get install -y sudo whois apt-utils 
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
##
# Add user and disable sudo password checking
RUN useradd \
  --groups=sudo,root \
  --create-home \
  --home-dir=/home/huezo \
  --shell=/bin/bash \
  --password=$(mkpasswd huezo) \
  huezo \
&& sed -i '/%sudo[[:space:]]/ s/ALL[[:space:]]*$/NOPASSWD:ALL/' /etc/sudoers

##################################
USER huezo
##################################


